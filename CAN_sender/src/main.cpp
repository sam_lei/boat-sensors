/*
Beispielprogramm zum Testen auf einem Aruino Nano.

Anschluss des Boards wie hier beschrieben: https://github.com/autowp/arduino-mcp2515

Programm sendet ein mal die Sekunde einen Ping durch den CAN mit ID = F6 und falls es etwas empfängt
sendet es eine CAN Nachricht mit ID = 36.
*/

#include <SPI.h>
#include <mcp2515.h>

struct can_frame canMsg;
struct can_frame canMsg1;
struct can_frame canMsg2;
MCP2515 mcp2515(10);

// Generally, you should use "unsigned long" for variables that hold time
// The value will quickly become too large for an int to store
unsigned long previousMillis = 0;        // will store last time LED was updated


void setup() {

  canMsg1.can_id  = 0x0F6;
  canMsg1.can_dlc = 8;
  canMsg1.data[0] = 0x8E;
  canMsg1.data[1] = 0x87;
  canMsg1.data[2] = 0x32;
  canMsg1.data[3] = 0xFA;
  canMsg1.data[4] = 0x26;
  canMsg1.data[5] = 0x8E;
  canMsg1.data[6] = 0xBE;
  canMsg1.data[7] = 0x86;

  canMsg2.can_id  = 0x036;
  canMsg2.can_dlc = 8;
  canMsg2.data[0] = 0x0E;
  canMsg2.data[1] = 0x00;
  canMsg2.data[2] = 0x00;
  canMsg2.data[3] = 0x08;
  canMsg2.data[4] = 0x01;
  canMsg2.data[5] = 0x00;
  canMsg2.data[6] = 0x00;
  canMsg2.data[7] = 0xA0;

  while (!Serial);
  Serial.begin(115200);
  SPI.begin();

  mcp2515.reset();
  // mcp2515.setBitrate(CAN_125KBPS);
  mcp2515.setBitrate(CAN_500KBPS, MCP_16MHZ);
  mcp2515.setNormalMode();

  Serial.println("Write every second to can on id 0x0F6 and respond to every incoming message on id 0x036 immediately.");
}

void loop() {

  // send alive message every second
  unsigned long currentMillis = millis();
  if (currentMillis - previousMillis >= 1000) {
    // save the last time
    previousMillis = currentMillis;

    mcp2515.sendMessage(&canMsg1);
    Serial.println("Alive-Messages sent (id = 0x0F6)");
  }

  // check for incoming message and send response
  if (mcp2515.readMessage(&canMsg) == MCP2515::ERROR_OK) {
    Serial.println("------- CAN Read ----------");
    Serial.println("ID  DLC   DATA");
    Serial.print(canMsg.can_id, HEX); // print ID
    Serial.print(" ");
    Serial.print(canMsg.can_dlc, HEX); // print DLC
    Serial.print(" ");

    for (int i = 0; i<canMsg.can_dlc; i++)  {  // print the data
      Serial.print(canMsg.data[i],HEX);
      Serial.print(" ");
    }
    Serial.println();
    // send response
    mcp2515.sendMessage(&canMsg2);
    Serial.println("Response-Messages sent (id = 0x036)");
  }

}
