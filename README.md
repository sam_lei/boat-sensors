# Segelyacht Informationssystem
Dieses Repository dient dazu, eine komplette Sensorsuit eines Bootes in OpenCPN
zu integreiren, bzw. einen automatisierten Überblick über alle Bootsdaten zu
verschaffen.

### Beitragen
1. Erstelle einen eigenen Branch mit ``git checkout -b dev/branch-name``
2. Erstelle einen Merge-Request

[Hier findest du eine Markdown-Anleitung um Dokumentation zu schreiben](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)

## Router
Modell: RUT955.

* Benutzername: ``admin``
* Passwort: ``Sandy2018``

Danach noch bei Netzwerk --> Firewall das NAT Häkchen be wan setzen und unter
Port Forwarding die SSH Regeln aktivieren. Sonst geht SSH nicht über WLAN.

### MQTT
Der Router fungiert als MQTT Broker, damit ist immer eine feste IP Andresse
vorhanden an die alle clients senden könne. Es ist kein Benutzername/Passwort
gesetzt.

* host: ``192.168.1.1``
* port: ``1883``

## Raspberry Pi (Bootsrechner)
Openplotter mit angepasster/angelegter /etc/resolv.conf.tail wo ``nameserver 192.168.1.1`` (IP vom Router) eingetragen wird. Sonst geht der Nameserver nicht. Kann man mit ``ping 8.8.8.8`` und ``ping google.com`` überprüfen. Anschließend muss noch die Zeit richtig eingestellt werden da sonst https/ssl nicht funktioniert.

### Shell
* Login: ``ssh pi@ip-des-raspberry``, Passwort: Sandy2018
* Config: ``sudo raspi-config``

### Real VNC (Remote Desktop)
Es wurde VNC-Server-Autostart eingerichtet.
* Host: ``lokale-ip-des-raspberry``
* Benutzer: ``pi``
* Passwort: ``Sandy2018``

### OpenPlotter
[Dokumentation (pdf)](https://legacy.gitbook.com/book/sailoog/openplotter-documentation/details)

## Karten

### OpenCPN (Plotter)
[Anleitung](https://opencpn.org/wiki/dokuwiki/doku.php)

Falls OpenCPN nach dem Hinzufügen von Karten nicht startet, in der shell anmelden und: ``rm -rf /home/pi/.opencpn``
Danach müssen alle Einstellungen neu vorgenommen werden.

### Open Sea Maps (Kartendaten)
[Downloadverzeichnis der einzelnen Gebiete - Gesammt 4.321 GB](https://wiki.openstreetmap.org/wiki/DE:KAP-charts_from_OpenSeaMap#Download)

# Sensoren
Hier ein Überblick über die Elemente und eine Verbindung untereinander.

![Schematischer Plan](schematics/boat-electronics.png)

## Windgeschwindigkeit
[WiKi](https://www.dfrobot.com/wiki/index.php/Wind_Speed_Sensor_Voltage_Type(0-5V)_SKU:SEN0170)

### Anschließen
Kontakt 1 ist plus (an 9V bis 24V externe Spannungsquelle anschließen), Kontakt 2 am Anemometer ist minus (an minus von externer Spannungsquelle UND an den Arduino/WeMos anschließen) und Kontakt 3 kommt an einen analogen Input vom Arduino/WeMos.

### Messwerte
Die Windgeschwindigkeit steigt proportional zur Spannung.

* 0V = 0m/s
* 2.5V = 15m/s
* 5V = 30m/s

### Einfaches Codebeispiel

```C++
int sensorValue = analogRead(A0);
float outvoltage = sensorValue * (5.0 / 1023.0);
int windspeed = 6 * outvoltage; // in m/s
```

## Windrichtung


## Kompass


## IMU
 [Ebay beispiel](https://www.ebay.de/itm/9-Achsen-Beschleunigungssensor-GYRO-Drehratensensor-Digitalkompass-GY-85/173373962999?hash=item285de4baf7:g:~kcAAOSwxllbK1E4:rk:1:pf:1&frcectupt=true)

## GPS/AIS Netzwerk
Idee: Sende Datenstrom über MQTT und emuliere eine Serielle NMEA Verbindung auf
dem Raspberry mit den eingehenden Daten. So erkennt OpenCPN die Geräte als lokal
angeschlossen automatisch.

## GPS
[Anleitung](https://raspberry.tips/raspberrypi-tutorials/gps-modul-mit-dem-raspberry-pi-ortung-und-navigation)
