/*
Programm empfängt auf dem CAN Bus mit 500kbps und 16MhZ.
Sobald eine Nachricht mit der ID = F6 empfangen wird, sendet es eine Nachricht mit der ID = BB zurück.

Pin-Belegung für WeMos D1 zu MCP2515:
VCC -> 5V/vin
GND -> GND
CS -> D8
SO -> D6 (MISO)
SI -> D7 (MOSI)
SCK -> D5 (SCK)
INT -> -

H -> CAN HIGH
L -> CAN LOW

Nicht vergessen: Quarz mit einem 16MhZ Quarz umlöten/tauschen!

TODO: Inverter Nachrichten senden.

Example: https://github.com/dexterbg/Twizy-Virtual-BMS/blob/master/src/TwizyVirtualBMS.h

Exemplarisch Init-Drv in Steps (Schritte) unterteilt:

1.    597/423 auf 00-F0-A2 / 03 einstellen
2.    warten auf 424/425 11 / 24
3.    …vermutlich kannst Du nun F0-A1 und F0-C1 überspringen und direkt…
4.    00-F4-C1 einstellen
5.    warten auf 11 / 2A
6.    00-95-41 einstellen (ist evtl. auch überflüssig) und loslegen

*/

#include "main.h"

// SETUP

void setup() {
	// set serial monitor baud rate and begin SPI protocoll
	// for communication with CAN adapter
	Serial.begin(115200);

  pinMode(D1, OUTPUT); //Setzte Dc/DcWandler
	pinMode(D3, OUTPUT); //Setzte Hauptschutz

	digitalWrite(D1, HIGH);
	digitalWrite(D3, LOW);


  //Verbinde mit WIFI
	setup_wifi();
	client.setServer(mqtt_server, mqtt_port);
	reconnect();


	SPI.begin();

	// set baud rate, frequency and mode for CAN communication
	mcp2515.reset();
	mcp2515.setBitrate(CAN_500KBPS, MCP_16MHZ);
	mcp2515.setNormalMode();
	Serial.println("CAN initialisiert");

	// build message 1b for BMS initialization
	msg1a.can_id = 0x0423;
	msg1a.can_dlc = 8;
	msg1a.data[0] = 0x03; // CHG --> BMS: ON
	// set other bytes? default 0x00 ?

	// build message 1a for BMS initialization
	msg1b.can_id = 0x0597;
	msg1b.can_dlc = 8; // standartlänge
	msg1b.data[0] = 0x00; // Netzstatus: 0 = nicht angeschlossen;
	msg1b.data[1] = 0xF0; // Status flag: 1111 0000 in binaer
	msg1b.data[3] = 0xA2; // Ladeprotokoll: Init
	msg1b.data[7] = 0x00; // Temp. ladegeraet, grad celsius - 40

	// build message 4 for BMS initialization
	msg4.can_id = 0x0597;
	msg4.can_dlc = 8; // standartlänge
	msg4.data[0] = 0x00; // Netzstatus: 0 = nicht angeschlossen;
	msg4.data[1] = 0xF4; // Status flag: 1111 0100
	msg4.data[3] = 0xC1; // Ladeprotokoll: Init
	msg4.data[7] = 0x00; // Temp. ladegeraet, grad celsius - 40

	// build message 6 for BMS initialization
	msg6.can_id = 0x0597;
	msg6.can_dlc = 8; // standartlänge
	msg6.data[0] = 0x00; // Netzstatus: 0 = nicht angeschlossen;
	msg6.data[1] = 0x95; // Status flag: 1001 0101 TODO: in 0597 tabelle entschluesseln
	msg6.data[3] = 0x41; // Ladeprotokoll: Init
	msg6.data[7] = 0x00; // Temp. ladegeraet, grad celsius - 40
}

// MAIN LOOP

void loop() {
	switch (step) {
		case 1: // 1. 597/423 auf 00-F0-A2 / 03 einstellen
			initStep1();
			step++;
			break;

		case 2:	// 2. warten auf 424/425 11 / 24
			if (initStep2()) step++;
			break;

		case 3: // 3. …vermutlich kannst Du nun F0-A1 und F0-C1 überspringen und direkt…
			initStep3();
			step++;
			break;

		case 4:	// 4. 00-F4-C1 einstellen
			initStep4();
			step++;
			break;

		case 5: // 5. warten auf 11 / 2A
			if (initStep5()) step++;
			break;

		case 6: // 6. 00-95-41 einstellen (ist evtl. auch überflüssig) und loslegen
			initStep6();
			step++;
			break;

		case 7: // Normale Schleife welche nach beendigung der Initialisierung ausgefuehrt wird
			// ignore messages without ID and ignore double messages
			if (mcp2515.readMessage(&canMsg) == MCP2515::ERROR_OK /*&& canMsg.can_id != 0x0*/ && canMsg.can_id != lastCanMsg.can_id) {
				lastMillisReceived = millis();
				printRawMsg(canMsg);
				decodeMsg(canMsg);
				lastCanMsg = canMsg;
			}

			keepBmsAlive();
			if (bmsIsAlive() == false) step = 1; // Fuehre Init erneut durch

			break;

		default: // fange ungueltige step-werte ab
			Serial.println("Ungueltiger Zustand");
			break;
	}
}

// METHODS

// STEPS

void initStep1() {
	Serial.println("1. 597/423 auf 00-F0-A2 / 03 einstellen");
	mcp2515.sendMessage(&msg1a);
	mcp2515.sendMessage(&msg1b);
	step++;

	Serial.println("2. warten auf 424/425 11 / 24");
}

bool initStep2() {
	if (mcp2515.readMessage(&canRes) == MCP2515::ERROR_OK) {
		// valid CAN message received
		// Serial.println("received valid CAN message from BMS");
		if (canRes.can_id == 0x0424 && canRes.data[0] == 0x11) {
			received2a = true;
			Serial.println("2. received 11 on id 0424 --> BMS Bereit");
		}
		if (canRes.can_id == 0x0425 && canRes.data[0] == 0x24) {
			received2b = true;
			Serial.println("2. received 24 on id 0425 --> BMS Bereit");
		}
		return received2a && received2b;
	}
}

void initStep3() {
	Serial.println("3. ueberspringen..");
}

void initStep4() {
	Serial.println("4. 00-F4-C1 einstellen");
	mcp2515.sendMessage(&msg4);

	Serial.println("5. warten auf 11 / 2A");
}

bool initStep5() {
		if (mcp2515.readMessage(&canRes) == MCP2515::ERROR_OK) {
			// valid CAN message received
			// Serial.println("received valid CAN message from BMS");
			if (canRes.can_id == 0x0424 && canRes.data[0] == 0x11) {
				received5a = true;
				Serial.println("5. received 11 on id 0424 --> BMS is ON!");
			}
			if (canRes.can_id == 0x0425 && canRes.data[0] == 0x2A) { // ist irgendwie 24 statt 2A..
				received5b = true;
				Serial.println("5. received 2A on id 0425");
			}
			return received5a;
		}
}

void initStep6() {
	Serial.println("6. 00-95-41 einstellen");
	mcp2515.sendMessage(&msg6);
	// Finished BMS initialization
	Serial.println("BMS fertig initialisiert, ab jetzt werden Meldungen des BMS angezeigt:");
	Serial.println("-----------------");
	lastMillisReceived = millis();
}

// CHECK

void keepBmsAlive() {
	// Halte das BMS am Leben und erkenne falls die Initialisierung erneut durchgefuehrt werden muss
	if (millis() - lastMillisSend >= interval) {
		lastMillisSend = millis();
		mcp2515.sendMessage(&msg1a);
	}
}

bool bmsIsAlive() {
	if (millis() - lastMillisReceived >= 500) {
		Serial.println("BMS ist ausgegangen, starte neu...");
		return false;
	}
	return true;
}

// HELPERS

void printRawMsg(can_frame msg) {
	Serial.print("ID: ");
	Serial.print(msg.can_id, HEX); // print ID
	Serial.print(" Size: ");
	Serial.print(msg.can_dlc, HEX); // print DLC (Länge des Nachrichteninhaltes)
	Serial.print(" Data: ");
	for (int i = 0; i < msg.can_dlc; i++)  {  // print the data
		Serial.print(msg.data[i], HEX);
		Serial.print(" ");
	}
	Serial.println();
}

unsigned int batteryPercentage;
unsigned int maxInPower;
unsigned int maxOutPower;
unsigned int stateOfHealth;
unsigned int chargePower;
unsigned int ladeStatus;
int currentPower;
unsigned int maxTemp;
unsigned int maxT = 0;
double voltage1;
char test;

void decodeMsg(can_frame msg) {
	switch (msg.can_id) {
		case 0x0155:
			batteryPercentage = twoBytes2int(msg.data[4], msg.data[5]) / 400;
			Serial.print("Akkuladezustand in \%: ");
			Serial.println(batteryPercentage);

			chargePower = msg.data[0]*300;
			Serial.print("Ladeleistung in Watt: ");
			Serial.println(chargePower);

			currentPower = msg.data[2] | ((char)(msg.data[1] << 4) >> 4) << 8;
			currentPower = (currentPower - 2000) * 16;

			Serial.print("Aktueller Strom in Watt: ");
			Serial.println(currentPower);

			if(millis()-lastPublished > 10000)
			{
				mqttPublishInt("bms/percentage", batteryPercentage);
				mqttPublishInt("bms/currentPower", currentPower);
				lastPublished = millis();
			}

			break;

		case 0x0424:
			maxInPower = msg.data[2] * 500;
			maxOutPower = msg.data[3] * 500;
			stateOfHealth = msg.data[5];
			Serial.print("Max. Ladeleistung in Watt: ");
			Serial.println(maxInPower);
			Serial.print("Max. Entladeleistung in Watt: ");
			Serial.println(maxOutPower);
			Serial.print("SOH in \%: ");
			Serial.println(stateOfHealth);

			if(msg.data[0] == 17 && maxInPower > 100 && maxOutPower > 100) {
				ladeStatus = 1;
				Serial.println("BMS Bereit...");
				if(millis()-lastPublishedStatus > 10000)
				{
					client.publish("bms/status", "BMS bereit.");
					lastPublishedStatus = millis();
					digitalWrite(D3, HIGH);
				}
			} else if(ladeStatus == 1){
				ladeStatus = 0;
			}
			else{
				Serial.println("BMS Fehler!!!");
				if(millis()-lastPublishedStatus > 10000)
				{
					client.publish("bms/status", "BMS Fehler! System abgeschaltet.");
					digitalWrite(D3, LOW);
					lastPublishedStatus = millis();
				}
			}

			if(millis()-lastPublishedPower > 10000)
			{
				mqttPublishInt("bms/maxIn", maxInPower);
				mqttPublishInt("bms/maxOut", maxOutPower);

				lastPublishedPower = millis();
			}

			break;

			case 0x0554:
				maxT = 0;
				for(int i = 0; i < 7; i++) {
						if((uint)msg.data[i] > maxT) maxT = (uint)msg.data[i];
				}
				maxTemp = maxT-40;
				Serial.println("Temp in Celsius: ");
				Serial.println(maxTemp);

				if(millis()-lastPublishedTemp > 10000 && maxTemp < 215)
				{
					mqttPublishInt("bms/temp", maxTemp);
					lastPublishedTemp = millis();
				}

			break;

			case 0x0556:
				voltage1 = (msg.data[0] << 4) | (msg.data[1] >> 4);
				voltage1 = voltage1 / 200.;
				Serial.print("Voltage1 in Volt: ");
				Serial.println(voltage1);

			break;


		default:
			Serial.println("Noch keine Dekodierung vorhanden.");
			break;

	}
}

unsigned int twoBytes2int(unsigned char a, unsigned char b) {
	return int((unsigned char)a << 8 | (unsigned char)b);
}

double threeBytes2double(unsigned char a, unsigned char b, unsigned char c) {
	return double((unsigned char)c | (unsigned char)b << 8 | (unsigned char)a << 16);
}

void setup_wifi() {
    delay(10);
    Serial.println();
    Serial.print("Connecting to ");
    Serial.println(SSID);

    WiFi.begin(SSID, PSK);
		int z = 0;
    while (WiFi.status() != WL_CONNECTED && z < 10) {
        delay(500);
        Serial.print(".");
				z++;
    }

    Serial.println("");
    Serial.println("WiFi connected");
    Serial.println("IP address: ");
    Serial.println(WiFi.localIP());
}

void reconnect() {
    if (!client.connected()) {
        Serial.println("Reconnecting MQTT...");
        if (!client.connect(mqtt_client_name, mqtt_user, mqtt_pass)) {
            Serial.print("failed, rc=");
            Serial.print(client.state());
            Serial.println(" retrying next round");
        }
    }

    // hier steht welche Topics alle subscribed werden
    //for(int i = 0; i < lenObjekte; i++)
    //{
    //     client.subscribe(Objekte[i].name2);
    //}
     if (client.connected()) Serial.println("MQTT Connected...");
    client.publish("hallo", "Hello World isch bin im Salon in der Küche"); //testkanal ob mit mqtt server verbunden
}

// Alternative zu 'pub': Samuel: send topic to the MQTT broker (accepts integer)
void mqttPublishInt(char *topic, int value) {
  if (client.connected()) {
    char buffer[7];
    dtostrf(value, 7, 0, buffer);
    client.publish(topic, buffer);
		Serial.println("gepublished");
  } else {
		setup_wifi();
		client.setServer(mqtt_server, mqtt_port);
		reconnect();
	}
}

// Alternative zu 'pub': Samuel: send topic to the MQTT broker (accepts integer)
void mqttPublishString(char *topic, char value[20]) {
  if (client.connected()) {
    client.publish(topic, value);
		Serial.println("gepublished");
  } else {
		setup_wifi();
		client.setServer(mqtt_server, mqtt_port);
		reconnect();
	}
}
